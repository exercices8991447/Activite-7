CREATE database IF NOT EXISTS todoList;
use todoList;

CREATE TABLE IF NOT EXISTS `todo` ( 
    `id` bigint(20) NOT NULL AUTO_INCREMENT, 
    `task` varchar(2048) NOT NULL, 
    `done` tinyint(1) NOT NULL DEFAULT '0', 
    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, 
    PRIMARY KEY (`id`) 
);