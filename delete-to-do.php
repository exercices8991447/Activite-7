<?php
session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $index = $_POST['index'];

    if (isset($_SESSION['todoList'][$index])) {
        unset($_SESSION['todoList'][$index]);
        $_SESSION['todoList'] = array_values($_SESSION['todoList']); // Reindex the array
        echo json_encode(['success' => true]);
    } else {
        echo json_encode(['success' => false, 'error' => 'Task not found']);
    }
} else {
    echo json_encode(['success' => false, 'error' => 'Invalid request method']);
}
?>
